let urls = {
    ".gpac-js": "/version/gpac_js_ver.txt",
    ".gpac-cs": "/version/gpac_cs_ver.txt",
    ".gpac-cs-date": "/version/gpac_cs_date.txt"
}

$(document).ready(() => {
    for (key in urls)
        load(urls[key], key)
})

let isDefined = (key) => { return $(key).length !== 0 }

let load = (url, key) => {
    if (isDefined(key)) {
        $.get(url, (data) => {
            $(key).text(data)
        })
    }
}