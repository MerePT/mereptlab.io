let check = setInterval(() => {
    if ($("#opening").length !== 0) {
        clearInterval(check)
        main()
    }
}, 100)

let isOnMobile = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
}

let appendAdmin = (id) => {
    $("#admin").hide()
    $(id).append($("#admin").html())
}

let main = () => {
    if (isOnMobile()) {
        $("#opening").hide()
        appendAdmin("#mobile")
        $("#mobile").show()
    } else {
        setTimeout(() => {
            window.location.assign('ts3server://merept.ts3.vip');
        }, 1000)
        var i = 6
        var t = setInterval(() => {
            $("#countdown").text((i - 1).toString()) 
            i--
            if (i === 0) {
                $("#countdown").hide()
                appendAdmin("#openbtn")
                $("#openbtn").fadeIn()
                clearInterval(t)
            }
        }, 1000)    
    }
}